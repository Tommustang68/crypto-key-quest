namespace Crypto_Key_Quest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addsouls : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Souls", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Souls");
        }
    }
}
