namespace Crypto_Key_Quest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Stringforlists : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "PathList", c => c.String());
            AddColumn("dbo.AspNetUsers", "ProgressionList", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "ProgressionList");
            DropColumn("dbo.AspNetUsers", "PathList");
        }
    }
}
