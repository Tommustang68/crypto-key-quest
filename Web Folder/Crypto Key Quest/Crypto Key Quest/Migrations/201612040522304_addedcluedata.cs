namespace Crypto_Key_Quest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedcluedata : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Selector", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "PastClue", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "CurrentClue", c => c.String());
            AddColumn("dbo.AspNetUsers", "Progression", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Progression");
            DropColumn("dbo.AspNetUsers", "CurrentClue");
            DropColumn("dbo.AspNetUsers", "PastClue");
            DropColumn("dbo.AspNetUsers", "Selector");
        }
    }
}
