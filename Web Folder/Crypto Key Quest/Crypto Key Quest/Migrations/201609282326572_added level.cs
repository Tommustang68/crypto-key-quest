namespace Crypto_Key_Quest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedlevel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "RoomLevel", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "RoomLevel");
        }
    }
}
