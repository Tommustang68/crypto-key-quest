namespace Crypto_Key_Quest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class cryptokeydata : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Points", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "CurrentPathIndex", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "CurrentGoal", c => c.Int(nullable: false));
            AddColumn("dbo.AspNetUsers", "CurrentPathCompleted", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "CurrentPathCompleted");
            DropColumn("dbo.AspNetUsers", "CurrentGoal");
            DropColumn("dbo.AspNetUsers", "CurrentPathIndex");
            DropColumn("dbo.AspNetUsers", "Points");
        }
    }
}
