﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace NameHelper
{
    public static class NameHelper
    {

        public static string getName(this IPrincipal usr)
        {
            var fullNameClaim = ((ClaimsIdentity)usr.Identity).FindFirst("Name");
            if (fullNameClaim != null)
                return fullNameClaim.Value;

            return "";
        }

    }
}