﻿using System.Web;
using System.Web.Mvc;

namespace Crypto_Key_Quest
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
