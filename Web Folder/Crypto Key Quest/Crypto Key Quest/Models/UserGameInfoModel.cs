﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Crypto_Key_Quest.Models
{
    public class PetersGameInfoModel
    {
        public int RoomLevel { get; set; }

    }


    public class CryptoGameInfoModel
    {

        public int[] PathList { get; set; }
        public int[] ProgressionList { get; set; }
       
    
        public int Points { get; set; }
        
        public int CurrentPathIndex { get; set; }
        public int CurrentGoal { get; set; }
        public int CurrentPathCompleted { get; set; }
    }
}