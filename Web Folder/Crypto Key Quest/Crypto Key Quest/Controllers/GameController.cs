﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Crypto_Key_Quest.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;

namespace Crypto_Key_Quest.Controllers
{
    //
    [Authorize(Roles = "Member")]
    public class GameController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public GameController()
        {
        }

        public GameController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        //Gets the game page with the data used to add to the browser storage
        // GET: Game/Index
        public ActionResult Index()
        {

            var userId = User.Identity.GetUserId();
            var model = new AddCryptoDataModel
            {

                Points = getPoints(),
                PathList = getPathList(),
                ProgressionList = getProgressionList(),
                CurrentPathIndex = getCurrentPathIndex(),
                CurrentGoal = getCurrentGoal(),
                CurrentPathCompleted = getCurrentPathCompleted(),
                Selector = getSelector(),
                PastClue = getPastClue(),
                CurrentClue = getCurrentClue(),
                Progression = getProgression()

            };

            return View(model);
        }

        //Gets the Test game page with the data used to add to the browser storage
        // GET: Game/TestGame
        public ActionResult TestGame()
        {

            var userId = User.Identity.GetUserId();
            var model = new AddCryptoDataModel
            {

                Points = getPoints(),
                PathList = getPathList(),
                ProgressionList = getProgressionList(),
                CurrentPathIndex = getCurrentPathIndex(),
                CurrentGoal = getCurrentGoal(),
                CurrentPathCompleted = getCurrentPathCompleted(),
                Selector = getSelector(),
                PastClue = getPastClue(),
                CurrentClue = getCurrentClue(),
                Progression = getProgression()

            };

            return View(model);
        }


        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        public ActionResult Help()
        {
            return View();
        }

        #region Helpers
        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        private int getPoints()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.Points;
            }
            return 0;
        }

        private string getPathList()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PathList;
            }
            return "";
        }

        private string getProgressionList()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.ProgressionList;
            }
            return "";
        }

        private int getCurrentPathIndex()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.CurrentPathIndex;
            }
            return 0;
        }

        private int getCurrentGoal()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.CurrentGoal;
            }
            return 0;
        }

        private int getCurrentPathCompleted()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.CurrentPathCompleted;
            }
            return 0;
        }

        private int getSelector()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.Selector;
            }
            return 0;
        }

        private int getPastClue()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PastClue;
            }
            return 0;
        }

        private string getProgression()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.Progression;
            }
            return "0";
        }

        private string getCurrentClue()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.CurrentClue;
            }
            return "0";
        }

        private string getName()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.Name;
            }
            return "";
        }

        private string getEmail()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.Email;
            }
            return "";

        }
        #endregion
    }
}