﻿//For Login page

function showPageLoadingSpinner() {
    //if valid then show loading div
    if (validateForm()) {
        $('#ajaxLoaderDiv').show();
        $('#submitButton').hide();
    }
}

//validate form check the id inputs
function validateForm() {

    if (document.getElementById('Email').value === '') {
        return false;
    }

    if (document.getElementById('Password').value === '') {
        return false;
    }

    return true;
}