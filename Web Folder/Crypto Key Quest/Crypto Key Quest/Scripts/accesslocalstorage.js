﻿//if not set defualt save is on
var _gameKey = "gamemakerstudio.438677409.";
var _userName = "default";

//used for the diffrent file names
var _selector = ".ClueSelector.sav";
var _currentClue = ".CurrentClue.sav";
var _currentGoal =".CurrentGoal.sav";
var _pathComleted = ".CurrentPathCompleted.sav";
var _pathIndex = ".CurrentPathIndex.sav";
var _pastClue =".PastClue.sav";
var _points = ".Points.sav";
var _progression = ".Progression.sav";


//sets the user name
function setUserName(name) {
    _userName = name;
}

//sets the game values in the view withthe hidden fields
function setGameValues(){

    var selector = getClueSelector();
    var currentClue = getCurrentClue();
    var currentGoal = getCurrentGoal(); 
    var pathComleted = getCurrentPathCompleted();
    var pathIndex = getCurrentPathIndex();
    var pastClue = getPastClue();
    var points = getPoints();
    var progression = getProgresion();

    document.getElementById('selector').value = selector;
    document.getElementById('currentClue').value = currentClue;
    document.getElementById('currentGoal').value = currentGoal;
    document.getElementById('pathComleted').value = pathComleted;
    document.getElementById('pathIndex').value = pathIndex;
    document.getElementById('pastClue').value = pastClue;
    document.getElementById('points').value = points;
    document.getElementById('progression').value = progression;

    //let them know game is saved
    alert("Your Game is Saved!");
    //after allert game goes to controller
}


// loads the game with all the parameters
// sets all the local storage files to the data from the model on call
function loadGame(points,currentPathIndex,currentPathCompleted,currentGoal,
    pastClue,clueSelector,currentClue,progression) {


    //first we clear loacal storage
    if (localStorage.length != 0) {
        localStorage.clear();
    }


    //then set local storage for game to see
    var keySelector = _gameKey + _userName + _selector;
    var valueSelector = clueSelector;
    localStorage.setItem(keySelector, valueSelector);

    var keyCurrentClue = _gameKey + _userName + _currentClue;
    var valueCurrentClue = currentClue;
    localStorage.setItem(keyCurrentClue, valueCurrentClue);

    var keyCurrentGoal = _gameKey + _userName + _currentGoal;
    var valueCurrentGoal = currentGoal;
    localStorage.setItem(keyCurrentGoal, valueCurrentGoal);

    var keyCurrentPathCompleted = _gameKey + _userName + _pathComleted;
    var valueCurrentPathCompleted = currentPathCompleted;
    localStorage.setItem(keyCurrentPathCompleted, valueCurrentPathCompleted);

    var keyCurrentPathIndex = _gameKey + _userName + _pathIndex;
    var valueCurrentPathIndex = currentPathIndex;
    localStorage.setItem(keyCurrentPathIndex, valueCurrentPathIndex);

    var keyPastClue = _gameKey + _userName + _pastClue;
    var valuePastClue = pastClue;
    localStorage.setItem(keyPastClue, valuePastClue);
    
    var keyPoints = _gameKey + _userName + _points;
    var valuePoints = points;
    localStorage.setItem(keyPoints, valuePoints);

    var keyProgression = _gameKey + _userName + _progression;
    var valueProgression = progression;
    localStorage.setItem(keyProgression, valueProgression);

}

//These are all the helpers used to get the data from each local storake file
//if cant find the file will set to a default value
function getClueSelector() {
    if (localStorage.length != 0) {

    var key = _gameKey + _userName + _selector;
    var value = localStorage.getItem(key);

    return value;
    } else {
        return 2;
    }  
}

function getCurrentClue() {
    if (localStorage.length != 0) {

        var key = _gameKey + _userName + _currentClue;
        var value = localStorage.getItem(key);
        return value;
    } else {
        return "WRJDVDUHSRSXODUKHUH";
    }
}

function getCurrentGoal() {
    if (localStorage.length != 0) {

        var key = _gameKey + _userName + _currentGoal;
        var value = localStorage.getItem(key);

        return value;
    } else {
        return 1;
    }
}

function getCurrentPathCompleted() {
    if (localStorage.length != 0) {

        var key = _gameKey + _userName + _pathComleted;
        var value = localStorage.getItem(key);

        return value;
    } else {
        return 0;
    }
}

function getCurrentPathIndex() {
    if (localStorage.length != 0) {

        var key = _gameKey + _userName + _pathIndex;
        var value = localStorage.getItem(key);
        return value;
    } else {
        return 0;
    }
}

function getPastClue() {
    if (localStorage.length != 0) {

        var key = _gameKey + _userName + _pastClue;
        var value = localStorage.getItem(key);

        return value;
    } else {
        return 20;
    }
}

function getPoints() {
    if (localStorage.length != 0) {

        var key = _gameKey + _userName + _points;
        var value = localStorage.getItem(key);

        return value;
    } else {
        return 25;
    }
}

function getProgresion() {
    if (localStorage.length != 0) {

        var key = _gameKey + _userName + _progression;
        var value = localStorage.getItem(key);

        return value;
    } else {
        return "1, 5, 7, 2, 4";
    }
}
