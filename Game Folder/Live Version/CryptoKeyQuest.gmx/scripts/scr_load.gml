///This is where the game executes any code needed to load a previous game session:


//Game state
var PointsFileName = scr_getusername() + ".Points.sav";
var ProgressionListFileName = scr_getusername() + ".Progression.sav";
var CurrentPathIndexFileName = scr_getusername() + ".CurrentPathIndex.sav";
var CurrentGoalFileName = scr_getusername() + ".CurrentGoal.sav";
var CurrentPathCompletedFileName = scr_getusername() + ".CurrentPathCompleted.sav";


//Clues
var PastClueFileName = scr_getusername() + ".PastClue.sav";
var CurrentClueFileName = scr_getusername() + ".CurrentClue.sav";
var ClueSelectorFileName = scr_getusername() + ".ClueSelector.sav";





if file_exists(PointsFileName){
    var LoadFile = file_text_open_read(PointsFileName);
    var value = file_text_read_real(LoadFile);
    file_text_close(LoadFile);
    //set global var
    global.points = value;
}


if file_exists(ProgressionListFileName){
    var LoadFile = file_text_open_read(ProgressionListFileName);
    var value = file_text_read_string(LoadFile);
    file_text_close(LoadFile);
    //set global var
    //show_message("list from file: "+ value);//debug
    var list = scr_convertestringtolist(value,",")
    global.progressionList = list;
}

if file_exists(CurrentPathIndexFileName){
    var LoadFile = file_text_open_read(CurrentPathIndexFileName);
    var value = file_text_read_real(LoadFile);
    file_text_close(LoadFile);
    //set global var
    global.currentPathIndex = value;
}


if file_exists(CurrentGoalFileName){
    var LoadFile = file_text_open_read(CurrentGoalFileName);
    var value = file_text_read_real(LoadFile);
    file_text_close(LoadFile);
    //set global var
    global.currentPathGoal = value;
}

if file_exists(CurrentPathCompletedFileName){
    var LoadFile = file_text_open_read(CurrentPathCompletedFileName);
    var value = file_text_read_real(LoadFile);
    file_text_close(LoadFile);
    //set global var
    global.currentPathCompleted = value;
}


//Clue Checkers
if file_exists(PastClueFileName){
    var LoadFile = file_text_open_read(PastClueFileName);
    var value = file_text_read_real(LoadFile);
    file_text_close(LoadFile);
    //set global var
    global.pastClue = value;
}

global.clueStablizer = true;

if file_exists(CurrentClueFileName){
    var LoadFile = file_text_open_read(CurrentClueFileName);
    var value = file_text_read_string(LoadFile);
    file_text_close(LoadFile);
    //set global var
    global.currentClue = value;
}

if file_exists(ClueSelectorFileName){
    var LoadFile = file_text_open_read(ClueSelectorFileName);
    var value = file_text_read_real(LoadFile);
    file_text_close(LoadFile);
    //set global var
    global.clueSelector = value;
}


