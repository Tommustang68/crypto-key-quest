///converts string to list(text, split) returns a list

//This is used for convering a dslist into a string

var list = ds_list_create();
var text = argument0;
var split = argument1;
var temp_str = "";

 
 
for (var i = 1; i <= string_length(text); i++) {
   temp_str += string_char_at(text, i);
   if (string_char_at(text, i+1) == split || i == string_length(text) ) {
      ds_list_add(list, temp_str);
      temp_str = "";
      i++;
   }
}


return list;
