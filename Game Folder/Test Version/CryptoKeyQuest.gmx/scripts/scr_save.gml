///This is the code the game uses to save a game session:


//get file names

var PointsFileName = scr_getusername() + ".Points.sav";
var ProgressionListFileName = scr_getusername() + ".Progression.sav";

var CurrentPathIndexFileName = scr_getusername() + ".CurrentPathIndex.sav";
var CurrentGoalFileName = scr_getusername() + ".CurrentGoal.sav";
var CurrentPathCompletedFileName = scr_getusername() + ".CurrentPathCompleted.sav";

//Clues
var PastClueFileName = scr_getusername() + ".PastClue.sav";
var CurrentClueFileName = scr_getusername() + ".CurrentClue.sav";
var ClueSelectorFileName = scr_getusername() + ".ClueSelector.sav";


//delete previous files
if file_exists(PointsFileName){
    file_delete(PointsFileName);
}


if file_exists(ProgressionListFileName){
    file_delete(ProgressionListFileName);
}

if file_exists(CurrentPathIndexFileName){
    file_delete(CurrentPathIndexFileName);
}
if file_exists(CurrentGoalFileName){
    file_delete(CurrentGoalFileName);
}

if file_exists(CurrentPathCompletedFileName){
    file_delete(CurrentPathCompletedFileName);
}

//Clue delete

if file_exists(PastClueFileName){
    file_delete(PastClueFileName);
}

if file_exists(CurrentClueFileName){
    file_delete(CurrentClueFileName);
}

if file_exists(ClueSelectorFileName){
    file_delete(ClueSelectorFileName);
}
//open files

var PointsFile = file_text_open_write(PointsFileName);
var ProgressionListFile = file_text_open_write(ProgressionListFileName);
var CurrentPathIndexFile = file_text_open_write(CurrentPathIndexFileName);
var CurrentGoalFile = file_text_open_write(CurrentGoalFileName);
var CurrentPathCompletedFile = file_text_open_write(CurrentPathCompletedFileName);

//clue files
var PastClueFile = file_text_open_write(PastClueFileName);
var CurrentClueFile = file_text_open_write(CurrentClueFileName);
var ClueSelectorFile = file_text_open_write(ClueSelectorFileName);

//get values

var progressionList = scr_convertlisttostring(global.progressionList); //This is a list in string form
var point = global.points;//This is the amount of point the player has.
var currentPathIndex = global.currentPathIndex; //This is the current index used to see if the player is at the right location.
var currentPathGoal = global.currentPathGoal; //This is the coordinate of the player's current goal point.
var currentPathCompleted = global.currentPathCompleted;//This is the variable that keeps track of if the player has won the game.

//clues
var pastClue = global.pastClue;
var currentClue = global.currentClue;
var clueSelector = global.clueSelector;



//write data to files

file_text_write_string(ProgressionListFile,progressionList);
file_text_write_real(PointsFile,point);
file_text_write_real(CurrentPathIndexFile,currentPathIndex);
file_text_write_real(CurrentGoalFile,currentPathGoal);
file_text_write_real(CurrentPathCompletedFile,currentPathCompleted);

//clues
file_text_write_real(PastClueFile,pastClue);
file_text_write_real(CurrentClueFile,currentClue);
file_text_write_real(ClueSelectorFile,clueSelector);

//close files

file_text_close(ProgressionListFile);
file_text_close(PointsFile);
file_text_close(CurrentPathIndexFile);
file_text_close(CurrentGoalFile);
file_text_close(CurrentPathCompletedFile);
file_text_close(PastClueFile);
file_text_close(CurrentClueFile);
file_text_close(ClueSelectorFile);
